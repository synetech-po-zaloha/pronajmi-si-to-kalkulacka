describe("fill form ", function () {
  it("fill and submit form ", function () {
    cy.visit("http://localhost:8000/contact/");
    cy.get('[name="dateFrom"]').type("29.06.2022");
    cy.get('[name="dateTo"]').type("30.06.2022");
    cy.get('[name="fullName"]').type("Lolita Havrychko");
    cy.get('[name="tel"]').type("+420774344528");
    cy.get('[name="email"]').type("lolitagavr@gmail.com");
    cy.get('[name="address"]').type("Krymska 84");
    cy.get("form").submit();
  });
});
