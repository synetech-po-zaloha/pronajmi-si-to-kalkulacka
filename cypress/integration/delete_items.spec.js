describe("delete item ", function () {
  it("open cart and delete item ", function () {
    cy.visit("http://localhost:8000/");
    cy.get('[data-test="add"]').first().click();
    cy.get('[data-test="remove"]').first().click();
    cy.get('[data-test="addButton"]').first().dblclick();
    cy.get('[data-test="add"]').last().click();
    cy.get('[data-test="addButton"]').last().click();
    cy.contains("Do košíku").click();
    cy.wait(3000);
    cy.get('[data-test="remove"]').first().click();
    cy.get('[data-testid="DeleteIcon"]').last().click();
    cy.contains("Pokračovat").click();
    cy.get('[name="dateFrom"]').type("29.06.2022");
    cy.get('[name="dateTo"]').type("30.06.2022");
    cy.get('[name="fullName"]').type("John Doe");
    cy.get('[name="tel"]').type("+420777558833");
    cy.get('[name="email"]').type("john.doe@testmail.com");
    cy.get('[name="address"]').type("Krymska 84");
    cy.get("form").submit();
  });
});
