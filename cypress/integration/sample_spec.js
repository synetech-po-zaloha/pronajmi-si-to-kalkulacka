describe("add goods", function () {
  it("open and add", function () {
    cy.visit("http://localhost:8000/");
    cy.get('[data-test="add"]').first().click();
    cy.get('[data-test="remove"]').first().click();
    cy.get('[data-test="addButton"]').first().dblclick();
    cy.get('[data-test="add"]').last().click();
    cy.get('[data-test="addButton"]').last().click();
  });
});
