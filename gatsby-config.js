const { execSync } = require("child_process");

if (process.env.CI !== "true") {
  execSync(
    `npm run openapi-generator-cli:fetchAPIForMainBackend -- -i ./swagger.json`
    // `npm run openapi-generator-cli:fetchAPIForMainBackend -- -i '${process.env.}'`
  );
}
const plugins = [
  "gatsby-plugin-image",
  "gatsby-plugin-react-helmet",
  "gatsby-plugin-sitemap",
  {
    resolve: "gatsby-plugin-manifest",
    options: {
      name: "PronajmiSiTo Kalkulačka",
      short_name: `PronajmiSiTo`,
      start_url: `/`,
      background_color: `#FFDE44`,
      theme_color: `#F0F0F0`,
      display: `standalone`,
      icon: "src/images/pronajmiSiTo-logo.jpg",
      lang: "cs",
    },
  },
  "gatsby-plugin-offline",
  "gatsby-plugin-sharp",
  "gatsby-transformer-sharp",
  {
    resolve: "gatsby-source-filesystem",
    options: {
      name: "images",
      path: "./src/images/",
    },
    __key: "images",
  },
  `gatsby-plugin-material-ui`,
  `gatsby-theme-material-ui`,
];

if (process.env.NODE_ENV !== "development")
  plugins.push("gatsby-plugin-eslint");

module.exports = {
  siteMetadata: {
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins,
};
