const fetch = (...args) =>
  import(`node-fetch`).then(({ default: fetch }) => fetch(...args));

exports.sourceNodes = async ({
  actions: { createNode },
  createContentDigest,
}) => {
  const result = await fetch("https://pronajmisito.cz/api/v1/category");
  const resultData = await result.json();

  createNode({
    preloadedCategories: Object.entries(resultData).map(([_, category]) => ({
      name: category.name,
      nameUrl: category["name-url"],
      order: category.order,
    })),
    id: `example-buildtime-categories`,
    parent: null,
    children: [],
    internal: {
      type: `categories`,
      contentDigest: createContentDigest(resultData),
    },
  });
};
