
<p align="center">
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Gatsby minimal starter
</h1>

File ve Figmě https://www.figma.com/file/HheVyb2XtlrY68wrOZ3F4Q/Pronajmisito---Kalkula%C4%8Dka
User stories v Epics.

## Mapa lidí

* Vratislav Zima projekt dotáhl a je PM
* Jersey je PO
* Radim Květ  hlavní developer
* Tadeáš Musil dohlížející a CR
* Lukáš Všetečka designér

## Další okolnosti

Projekt Je Finančně Priskrce, Jde O Barter Výsledek nemusí Odpovídat Tolik Figmě. 

## Architektura

Projekt zařizujeme téměř od Á do Z. Web je Reactí statickou aplikaci s některými prvky PWAčka, je možné ji instalovat jak na iOS tak na Android. Doména je zakoupena skrz Netlify kde i běží produkce. Aplikace odesílá emaily, kdo by si rád co zakoupil, jak na email objednávajícího tak na email info@pronajmisito.cz. Emaily posíláme skrz backend EmailJS, který bude (jestli tomu tak stále není, tak je stále napojen na tadeas.musil@synetech.cz) napojen na Gmail pronajmisita. Gmail pronajmisita jsme nezařizovali.

## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the minimal starter.

    ```shell
    # create a new Gatsby site using the minimal starter
    npm init gatsby
    ```

2.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    cd my-gatsby-site/
    npm run develop
    ```

3.  **Open the code and start customizing!**

    Your site is now running at http://localhost:8000!

    Edit `src/pages/index.js` to see your site update in real-time!

4.  **Learn more**

    - [Documentation](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Tutorials](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Guides](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [API Reference](https://www.gatsbyjs.com/docs/api-reference/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Plugin Library](https://www.gatsbyjs.com/plugins?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Cheat Sheet](https://www.gatsbyjs.com/docs/cheat-sheet/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

## 🚀 Quick start (Gatsby Cloud)

Deploy this starter with one click on [Gatsby Cloud](https://www.gatsbyjs.com/cloud/):

[<img src="https://www.gatsbyjs.com/deploynow.svg" alt="Deploy to Gatsby Cloud">](https://www.gatsbyjs.com/dashboard/deploynow?url=https://github.com/gatsbyjs/gatsby-starter-minimal)
