import { useQuery } from "@tanstack/react-query";
import CategoryContext, {
  Category as TCategory,
} from "../context/CategoryContext";
import { useContext } from "react";
import APIContext from "../context/APIContext";

type Item = {
  name: string;
  price: number;
  imagePath: string;
};

export const useCategory = (id: string) => {
  const { categoryApi } = useContext(APIContext);
  return useQuery(["item", id], async () => {
    const result = await categoryApi.getCategoryId({
      id,
    });
    return Object.entries(result).map(
      ([_, item]): Item => ({
        name: item.name,
        price: item.price,
        imagePath: item.mainPhotoPath,
      })
    );
  });
};

export const useCategories = () => {
  const { categoryApi } = useContext(APIContext);
  return useQuery(["categories"], async () => {
    const result = await categoryApi.getCategory();
    return Object.entries(result).map(
      ([index, item]): TCategory => ({
        name: item.name,
        url: item["name-url"],
        order: item.order,
        id: index,
      })
    );
  });
};
