import * as React from "react";
import { createContext, ReactNode, useState } from "react";

interface ICategoryContext {
  changeCategory: (newCategory: Category) => void;
  currentCategory: Category;
}

export type Category = {
  name: string;
  url: string;
  order: number;
  id: string;
};

const CategoryContext = createContext<ICategoryContext>({
  changeCategory: () => {
    throw new Error("Unexpected call of undefined method");
  },
  currentCategory: null,
});

export default CategoryContext;

export function CategoryContextProvider({ children }: { children: ReactNode }) {
  const [currentCategory, setCurrentCategory] = useState<Category>({
    name: "",
    url: "",
    order: 0,
    id: "0",
  });

  return (
    <CategoryContext.Provider
      value={{
        changeCategory(newCategory) {
          setCurrentCategory(newCategory);
        },
        currentCategory,
      }}
    >
      {children}
    </CategoryContext.Provider>
  );
}
