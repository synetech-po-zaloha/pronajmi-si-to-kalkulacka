import * as React from "react";
import { Collapse, MenuItem, MenuList } from "@mui/material";
import { useState } from "react";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

interface INestedMenuItemProps {
  item: CategoryItem;
  indent: number;
  categorySelected: (item: CategoryWithoutSubcategories) => void | unknown;
}

export type CategoryWithoutSubcategories = {
  name: string;
  url: string;
  order: number;
  id: number;
};

export type CategoryWithSubcategories = {
  name: string;
  id: number;
  subcategories: CategoryItem[];
};

export type CategoryItem =
  | CategoryWithSubcategories
  | CategoryWithoutSubcategories;

const NestedMenuItem = (props: INestedMenuItemProps) => {
  const Component = !!props.item.subcategories ? MultiLevel : SingleLevel;
  return (
    <Component
      item={props.item}
      indent={props.indent}
      categorySelected={props.categorySelected}
    />
  );
};

export default NestedMenuItem;

const SingleLevel = (props: {
  item: CategoryWithoutSubcategories;
  indent: number;
  categorySelected: (item: CategoryWithoutSubcategories) => void;
}) => {
  return (
    <MenuItem
      sx={{
        mr: props.indent,
        pl: props.indent + 2,
        py: props.indent == 0 ? "auto" : 1.5,
      }}
      onClick={() => props.categorySelected(props.item)}
    >
      {props.item.name}
    </MenuItem>
  );
};

const MultiLevel = (props: {
  item: CategoryWithSubcategories;
  indent: number;
  categorySelected: (item: CategoryWithoutSubcategories) => void;
}) => {
  const { subcategories: children } = props.item;
  const [open, setOpen] = useState(false);

  return (
    <>
      <MenuItem
        onClick={() => setOpen((oldIsOpen) => !oldIsOpen)}
        sx={{
          pl: props.indent + 2,
          py: props.indent == 0 ? "auto" : 1.5,
        }}
      >
        {props.item.name}
        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
      </MenuItem>
      <Collapse
        in={open}
        timeout="auto"
        unmountOnExit
        sx={{
          background: props.indent == 0 ? "background.default" : "#1A1917",
          width: "100%",
        }}
      >
        <MenuList>
          {children.map((child: CategoryItem) => (
            <NestedMenuItem
              key={child.name}
              item={child}
              indent={props.indent + 1}
              categorySelected={(item: CategoryWithoutSubcategories) => {
                setOpen(false);
                props.categorySelected(item);
              }}
            />
          ))}
        </MenuList>
      </Collapse>
    </>
  );
};
