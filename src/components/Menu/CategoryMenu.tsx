import * as React from "react";
import { useContext, useEffect, useState } from "react";
import {
  Button,
  Drawer,
  Menu,
  MenuItem,
  MenuList,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import NestedMenuItem, {
  CategoryWithoutSubcategories,
  CategoryItem,
} from "./NestedMenuItem";
import { KeyboardArrowDown, KeyboardArrowUp } from "@mui/icons-material";
import { Button as GatsbyButton } from "gatsby-theme-material-ui";
import { useTheme } from "@mui/material/styles";
import { useQueryParam, StringParam } from "use-query-params";
import { graphql, useStaticQuery } from "gatsby";
import CategoryContext, {
  Category as TCategory,
} from "../../context/CategoryContext";
import { useCategories } from "../../hooks/queryHooks";

const menuDownText = "Všechny kategorie";

interface IMobileMenu {
  curCatName: string | null | undefined;
  apiData: Array<TCategory>;
  onCategorySelect: (category: CategoryWithoutSubcategories) => void;
}

interface IDesktopMenu {
  item: CategoryItem;
  onCategorySelect: (category: CategoryWithoutSubcategories) => void;
}

const CategoryMenu = () => {
  const gatsbyResponse = useStaticQuery(graphql`
    query {
      categories {
        preloadedCategories {
          name
          order
          nameUrl
        }
      }
    }
  `);
  const theme = useTheme();
  const { currentCategory, changeCategory } = useContext(CategoryContext);

  const [curCatName, setCurCatName] = useQueryParam("category", StringParam);
  const [categoryData, setCategoryData] = useState<TCategory[]>(
    gatsbyResponse.categories.preloadedCategories
  );
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const result = useCategories();
  useEffect(() => {
    if (result.isSuccess) {
      const categories = result.data; // take care of the case when we have category set in query from page reload (category sharing)
      if (!(curCatName?.length > 0)) {
        // Category is not specified in URL.
        // Case where we load the page for the first time, so even category object is not specified.
        if (!currentCategory || currentCategory.name === "") {
          const defaultCategory = categories.find(
            (category) => category.id == 1
          );
          changeCategory({
            name: defaultCategory.name,
            url: defaultCategory.url,
            order: defaultCategory.order,
            id: "1",
          });
        }
      }
      setCategoryData(categories);
    }
  }, [result]);

  useEffect(() => {
    // Update the category in URL if we changed the category object.
    // This is needed for case when we load the page without any category in URL,
    // and we set the category object for the first time.
    // But this covers even the case where we return to main site from some other part of the app.
    setCurCatName(currentCategory.name);
  }, [currentCategory]);

  return (
    <>
      {!categoryData && (
        <Stack direction="row" alignItems="center" justifyContent="center">
          <Typography variant="body2">Vyčkejte prosím</Typography>
        </Stack>
      )}
      {isMobile ? (
        <>
          {categoryData && (
            <MobileMenu
              curCatName={curCatName}
              apiData={categoryData}
              onCategorySelect={(item) => {
                changeCategory({
                  name: item.name,
                  url: item.url,
                  order: item.order,
                  id: item.id,
                });
              }}
            />
          )}
        </>
      ) : (
        <Stack direction="row" alignItems="center" justifyContent="center">
          {categoryData &&
            categoryData.map((item) => (
              <DesktopMenuItem
                item={item}
                key={item.name}
                onCategorySelect={(item) => {
                  changeCategory({
                    name: item.name,
                    url: item.url,
                    order: item.order,
                    id: item.id,
                  });
                }}
              />
            ))}
        </Stack>
      )}
    </>
  );
};

const MobileMenu = ({ curCatName, apiData, onCategorySelect }: IMobileMenu) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  return (
    <>
      <Button
        variant="text"
        onClick={() => setIsMenuOpen(true)}
        endIcon={<KeyboardArrowDown color="primary" />}
        sx={{
          color: "#FFF",
        }}
      >
        {isMenuOpen ? menuDownText : curCatName}
      </Button>
      <Drawer
        open={isMenuOpen}
        onClose={() => setIsMenuOpen(false)}
        anchor="top"
      >
        <MenuList
          sx={{
            background: "#1A1917",
            color: "white",
          }}
        >
          {apiData.map((item) => (
            <NestedMenuItem
              item={item}
              indent={0}
              key={item.name}
              categorySelected={(item) => {
                setIsMenuOpen(false);
                onCategorySelect(item);
              }}
            />
          ))}
        </MenuList>
      </Drawer>
    </>
  );
};

const DesktopMenuItem = ({ item, onCategorySelect }: IDesktopMenu) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const isOpen = anchorEl !== null;
  let DesktopButton: typeof GatsbyButton | typeof Button = GatsbyButton;

  return (
    <>
      {item.subcategories ? (
        <>
          <Button
            variant="text"
            onClick={(e) => setAnchorEl(e.currentTarget)}
            endIcon={
              isOpen ? (
                <KeyboardArrowUp color="primary" />
              ) : (
                <KeyboardArrowDown color="primary" />
              )
            }
            sx={{
              color: "#FFF",
            }}
          >
            {item.name}
          </Button>
          <Menu
            anchorEl={anchorEl}
            open={isOpen}
            onClose={() => setAnchorEl(null)}
          >
            {item.subcategories.map((child: CategoryWithoutSubcategories) => (
              <MenuItem
                key={child.name}
                onClick={() => {
                  setAnchorEl(null);
                  onCategorySelect(child);
                }}
              >
                {child.name}
              </MenuItem>
            ))}
          </Menu>
        </>
      ) : (
        <DesktopButton
          to="/"
          variant="text"
          sx={{
            color: "#FFF",
          }}
          onClick={() => {
            setAnchorEl(null);
            onCategorySelect(item);
          }}
        >
          {item.name}
        </DesktopButton>
      )}
    </>
  );
};

export default CategoryMenu;
