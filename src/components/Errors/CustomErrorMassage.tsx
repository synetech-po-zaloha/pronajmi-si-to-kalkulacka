import * as React from "react";
import { ErrorMessage } from "formik";
import { Typography } from "@mui/material";

const CustomErrorMassage = ({ name, error }) => {
  return (
    <ErrorMessage
      name={name}
      component={() => (
        <Typography sx={{ color: "error", fontSize: "12px" }}>
          {error}
        </Typography>
      )}
    />
  );
};

export default CustomErrorMassage;
